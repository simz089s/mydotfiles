#[cola]
#	startupmode = folder
#	logdate = iso8601
#	fontdiff = Fira Code,13,-1,5,50,0,0,0,0,0
#	theme = flat-light-blue
#	icontheme = light
#	statusshowtotals = true
#	blameviewer = git gui blame
#[gui]
#	editor = notepad++
#	historybrowser = gitk
[diff]
#	tool = difftastic
	tool = default-difftool
[difftool "difftastic"]
[difftool "default-difftool"]
	cmd = difft "$LOCAL" "$REMOTE"
[difftool]
	prompt = false
[pager]
	difftool = true
[merge]
	tool = code
[mergetool "code"]
#	cmd = code --merge $MERGED
	cmd = code --wait --merge $REMOTE $LOCAL $BASE $MERGED
[core]
	editor = code --wait
