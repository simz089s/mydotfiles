#if [ $TILIX_ID ] || [ $VTE_VERSION ]; then
#    source /etc/profile.d/vte.sh
#fi

# >>> mamba initialize >>>
# !! Contents within this block are managed by 'mamba init' !!
export MAMBA_EXE='/home/simon/.local/bin/micromamba';
export MAMBA_ROOT_PREFIX='/home/simon/micromamba';
__mamba_setup="$("$MAMBA_EXE" shell hook --shell bash --root-prefix "$MAMBA_ROOT_PREFIX" 2> /dev/null)"
if [ $? -eq 0 ]; then
 eval "$__mamba_setup"
else
 alias micromamba="$MAMBA_EXE"  # Fallback on help from mamba activate
fi
unset __mamba_setup
# <<< mamba initialize <<<

# fzf
. /usr/share/doc/fzf/examples/key-bindings.bash
. /usr/share/bash-completion/completions/fzf

# bat
export BAT_THEME="Solarized (light)"

# Python env default
micromamba activate base

# Zellij
#eval "$(zellij setup --generate-auto-start bash)"
#if [[ -z "$ZELLIJ" ]]; then
#    if [[ "$ZELLIJ_AUTO_ATTACH" == "true" ]]; then
#        zellij attach -c
#    else
#        zellij
#    fi
#
#    if [[ "$ZELLIJ_AUTO_EXIT" == "true" ]]; then
#        exit
#    fi
#fi

# Starship
#export CONDA_DEFAULT_ENV=base
eval "$(starship init bash)"

# Java
export JAVA_HOME=$(dirname $(dirname $(update-alternatives --query java | grep 'Value:' | sed 's/Value: //')))

# Maven
export PATH="/opt/apache/maven/bin${PATH:+:${PATH}}"
